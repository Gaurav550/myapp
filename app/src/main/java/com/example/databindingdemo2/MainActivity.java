package com.example.databindingdemo2;


import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.databindingdemo2.databinding.ActivityMainBinding;

//file created
public class MainActivity extends AppCompatActivity {
    ActivityMainBinding activityMainBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        activityMainBinding.setCustomAdapter(new RecyclerAdapter());
        activityMainBinding.setActivity(this);
        activityMainBinding.setUser(new UserClass("hh","77"));
    }

    public void call(View view, UserClass name) {
        Toast.makeText(this, "name" + name.id, Toast.LENGTH_LONG).show();
    }
}
