package com.example.databindingdemo2;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.databindingdemo2.databinding.ItemBinding;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

    List<UserClass> list ;

    RecyclerAdapter() {
        list  = new ArrayList<>();
        list.add(new UserClass("Jatayu", "101"));
        list.add(new UserClass("Rahul", "102"));
        list.add(new UserClass("Gaurav", "103"));
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemBinding itemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item, parent, false);

        return new MyViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
    UserClass userClass = list.get(position);
    holder.bindData(userClass);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ItemBinding itemBinding;

        public MyViewHolder(@NonNull ItemBinding itemView) {
            super(itemView.getRoot());
            itemBinding = itemView;

        }

        void bindData(Object o) {
           itemBinding.setVariable(BR.userClass, (UserClass) o);
            itemBinding.executePendingBindings();
        }
    }
}
