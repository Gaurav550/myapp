package com.example.databindingdemo2;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

public class UserClass extends BaseObservable {
    String name;
    String id;

     UserClass(String name, String id) {
        this.name = name;
        this.id = id;
    }


    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }

    @Bindable
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }
}